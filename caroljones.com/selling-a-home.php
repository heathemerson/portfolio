<?php include('includes/header.php'); ?>
<div class="container_12">
    <h2 class="center">Selling A Home</h2>
</div>
<hr />
<div class="container_12">
    <div class="grid_4 subcontent-image">
	    <img src="http://placehold.it/270x300" alt="" />
	</div>
    <div class="grid_7 pad-left">
	    <h5 class="avenir-bold">Selling A Home</h5>
	    <p>If you're planning to sell your home, look no further than CJR. Through market knowledge and experience, we can advise you on the best price to get your property sold in your timeframe.</p>
        <p>Even before your agent places the "For Sale" sign in your yard, they will have developed a marketing plan for your property. Your agent will enlist your help with maintaining the home for showings and Open Houses opportunities. And when that one buyer (because that's all you need – just one buyer) walks into your home and decides to make a purchase offer, your agent will be by your side with expert advice and negotiating skills.</p>
        <p>Whether it's making sure your property stands out amongst all of the other homes for sale or helping you with relocation after your home is sold, our agents are always focused on what's best for you.</p>
        <p>So give us a call or send us an email today to see how CJR can make sure you sell your home at the right price &mdash; right now.</p>
	</div>
</div>
<?php include('includes/footer.php'); ?>