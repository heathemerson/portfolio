<?php include('includes/header.php'); ?>
<div class="container_12">
    <h2 class="center">Home Warranty</h2>
</div>
<hr />
<div class="container_12">
    <div class="grid_4 subcontent-image">
	    <img src="http://placehold.it/270x300" alt="" />
	</div>
    <div class="grid_7 pad-left">
	    <h5 class="avenir-bold">Home Warranty With Carol Jones</h5>
	    <p>Nobody likes unplanned expenses; especially when you’re trying to sell your home or you’ve just moved into your new home. To help make sure no unplanned home expenses sneak up on you, CJR offers an unrivaled home warranty service through <a href="http://www.onlinehsa.com/">HSA Home Warranty</a>.</p>
        <p>An HSA Home Warranty eliminates the uncertainty of unplanned expenses by providing repair or replacement of covered items, all with a reasonable deductible. Whether you’re buying or selling a home, CJR and HSA Home Warranty have you covered.</p>
	</div>
</div>
<hr />
<div class="container_12">
    <h5 class="center">Benefits for Sellers</h5>
    <ul class="bul-list grid_10 push_1">
        <li>An HSA Home Warranty protects you while your home is listed for up to one year by covering system and appliance failures that occur during the listing period. This lets you focus on the most important thing &ndash; selling your home.</li>
        <li>An HSA Home Warranty can help you sell the house faster, while justifying a higher asking price.</li>
        <li>If an unexpected problem occurs after the sale, the buyer can turn to HSA rather than you for the resolution. And you can feel good knowing that HSA will protect your integrity by taking care of those problems without a hassle.</li>
    </ul>
</div>
<hr />
<div class="container_12">
    <h5 class="center">Benefits for Buyers</h5>
    <ul class="bul-list grid_10 push_1">
        <li>An HSA Home Warranty protects your budget by avoiding costly and unexpected repair expenses, especially during the purchase of your new home.</li>
        <li>An HSA Home Warranty provides the convenience of a single source for almost all repair needs. Prompt, reliable service is available 24 hours a day, 7 days a week.</li>
        <li>Your warranty is renewable annually, so you’re always prepared for the unexpected.</li>
    </ul>
</div>
<?php include('includes/footer.php'); ?>