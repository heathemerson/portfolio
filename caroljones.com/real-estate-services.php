<?php include('includes/header.php'); ?>
<div class="container_12">
    <h2 class="center">Real Estate Services</h2>
</div>
<hr />
<div class="container_12">
    <div class="grid_3 center">
        <a href="/pages/buying-a-home" class="no-decor">
        <img class="rounded-img" src="http://placehold.it/220x220" alt="" />
        <h5 class="avenir-bold">Buying a Home</h5>
        <p>Everything you need to help you find, buy, and move into your new home... <br /> And then some.</p>
        </a>
    </div>
    <div class="grid_3 center">
        <a href="/pages/selling-a-home" class="no-decor">
        <img class="rounded-img" src="http://placehold.it/220x220" alt="" />
        <h5 class="avenir-bold">Selling a Home</h5>
        <p>Whether it's a change in lifestyle or just a change in scenery, the resources you need to sell your home so that you can focus on more important things.</p>
        </a>
    </div>
    <div class="grid_3 center">
        <a href="/pages/relocation-services" class="no-decor">
        <img class="rounded-img" src="http://placehold.it/220x220" alt="" />
        <h5 class="avenir-bold">Relocation</h5>
        <p>Whether you’re moving across town or moving to a whole new community, CJR's relocation service ensures that you'll never have to stress about moving.</p>
        </a>
    </div>
    <div class="grid_3 center">
        <a href="http://www.cjrcommercial.com/" target="_blank" class="no-decor">
        <img class="rounded-img" src="http://placehold.it/220x220" alt="" />
        <h5 class="avenir-bold">CJR Commercial</h5>
        <p>A great source for Commercial Real Estate in Southwest Missouri. Search for properties, find an agent and more!</p>
        </a>
    </div>
</div>
<hr />
<div class="container_12">
	<div class="grid_8">
	    <h6 class="center">HSA Home Warranty and You ... Together, We're Better!</h6>
	    <p class="center">HSA provides comprehensive, affordable home warranty coverage insurace
with customer service that's second to none. Learn more...</p>
	</div>
    <div class="grid_4 subcontent-image">
	    <img src="images/hsa-warranty.jpg" alt="CJR Veterans to Realtors Program.">
	</div>
</div>
<hr />
<?php include('includes/footer.php'); ?>