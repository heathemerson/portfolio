<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head><!-- Build Number: 21.3.306 | Server: SWBRWSTG-OLA01 | Design: /RECoTemplates/Subpage14 -->

<meta content="Search for Homes in Missouri | CJR Carol Jones Realtors" name="title" />
<meta content="15 yr fixed, 30 yr fixed, missouri mortgage rates, springfield mo real estate, homes for sale, houses for sale, realtors, homes for sale in the, sale homes, for sale homes, realty, property for sale, sale houses, realtors, realtors in" name="keywords" />
<meta content="Find homes for sale, find an agent, view virtual tours, receive homes by e-mail, learn about buying and selling a home and more!" name="description" />
<meta content="Search for real estate and homes in your area!" name="abstract" />
<meta content="info@caroljones.com" name="reply-to" />
<meta content="noodp, noydir" name="robots" />
<meta content="All Contents Copyright &amp;#169; 2004 Carol Jones Realtors.  All Rights Reserved.The programming and software materials herein are copyright Real Estate Digital (RED). The programming and software materials are owned, held, or licensed by RED. Personal, educational, non-commercial, commercial or any other use of these materials, without the written permission of the RED, is strictly prohibited." name="copyright" />
<meta content="text/html; charset=UTF-8" http-equiv="content-type" />

<link href="/" rel="canonical" /><!-- canonical -->

<link href="http://caroljones.rdeskwebsite-stage.com/jscss/21.3.306/css/fe27bac8-9286-402c-9efa-c3a3abd9d69b" rel="stylesheet" type="text/css" /><!-- css-global -->
<link href="http://caroljones.rdeskwebsite-stage.com/jscss/21.3.306/css/6f09b613-6714-4f12-a617-dc722a678326" rel="stylesheet" type="text/css" /><!-- css-reco -->

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script><!-- js-global-ext-1 -->
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js"></script><!-- js-global-ext-2 -->
<script type="text/javascript" src="http://caroljones.rdeskwebsite-stage.com/jscss/21.3.306/js/949ec361-9ad9-4138-ac33-19ed26d32dad"></script><!-- js-global-int -->
<script type="text/javascript" src="http://caroljones.rdeskwebsite-stage.com/jscss/21.3.306/js/60d41b26-d839-41da-b163-20eb76d96ec9"></script><!-- js-page-int -->
<script type="text/javascript">
<!--//<![CDATA[
Utils.AppPath = '';
                  Utils.PublicAppName = '';
                  Utils.PublicHostHeader = 'caroljones.rdeskwebsite-stage.com';
                  if (typeof rbw == 'undefined') { var rbw = {} }
                  if (typeof rbw.utils == 'undefined') { rbw.utils = {} }
                  rbw.utils.version = '21.3.306';
                  if (typeof rbw.context == 'undefined') { rbw.context = {} }
                  rbw.context.Listing = null;
                  rbw.context.mapProvider = 'jqbingmap';
                  rbw.context.branding = 'RECo';
                  rbw.context.validStamp = 'ibrsv5pC0wKJiHcKpeJJyNnQlG4Jh0VJKWUeMp8qU0Y=';
                  rbw.context.useOfficeApi = false;
                  rbw.context.isAuthenticated = false;
                  jQuery(function() { try { jQuery('form').append('<input type="hidden" name="validstamp" value="ibrsv5pC0wKJiHcKpeJJyNnQlG4Jh0VJKWUeMp8qU0Y=" />'); } catch(err) { } });
var BulletOn = new Image();
                            BulletOn.src = 'http://caroljonesimages-stage.fnistools.com/images/RECos/35002/dropdown_bullet.gif';
                            var BulletOff = new Image();
                            BulletOff.src = 'http://caroljonesimages-stage.fnistools.com/images/RECos/35002/fill.gif';
                            var orientation = '';
//]]>-->
</script><!-- js-page-block -->

<meta http-equiv="X-UA-Compatible" content="IE=EDGE" /> <!-- Do not remove - Prevents ie9 from going into compatibility mode for new users -->
<meta name="google-site-verification" content="sLdn4xsUsJbrTZ4dO-oZjfKhL2D0oIT4sZEM0Eaq7zE" />

<link rel="shortcut icon" href="http://caroljones.rdeskwebsite-stage.com/images/recos/35002/favicon.ico" />

<!--[if gte IE 9]>
  <style type="text/css">
    * {
       filter: none;
    }
  </style>
<![endif]-->

    <link rel="stylesheet" href="stylesheets/app.css" />
    <script src="javascripts/vendor/custom.modernizr.js"></script>
</head>
<body>


<div class="page-bg">
    <div class="bg-white-trans-80">
        <div class="container_12">
        	<div class="grid_4">
        		<a href="/"><img class="cjr-logo" src="http://caroljonesimages-stage.fnistools.com/images/uploads/Graphics/6/35002_9911_cjr-logo.png" alt="Carol Jones Realtors" /></a>
        	</div>
            <div class="grid_5 push_3">
        		<p class="home-alert-txt"><span>My Home Alert: </span>
        		    <a href="">Sign In</a> | 
        		    <a href="">Sign Up</a>
                </p>
                <div class="social-media-icons">
                    <a href="#rss"><span class="rss"></span></a>
                    <a href="#facebook"><span class="facebook"></span></a>
                    <a href="#twitter"><span class="twitter"></span></a>
                    <a href="#linkedin"><span class="linkedin"></span></a>
                    <a href="#myspace"><span class="myspace"></span></a>
                    <a href="#other"><span class="other"></span></a>
                    <a href="#youtube"><span class="youtube"></span></a>
                    <a href="#tumblr"><span class="tumblr"></span></a>
                    <a href="#pinterest"><span class="pinterest"></span></a>
                </div>
        	</div>
        </div>
    </div>
    <div class="clear"></div>
	<ul class="main-nav">
	    <li><a href="/Listing/ListingSearch.aspx?clear=1">Find a Property</a></li>
	    <li><a href="/AgentSearch/Search.aspx?">Agents/Offices</a></li>
	    <li><a href="/pages/real-estate-services">Real Estate Services</a></li>
	    <li><a href="https://www.hslmo.com/hslmo/" target="_blank">Mortgage Services</a></li>
	    <li><a href="/pages/neighborhood-information">Area Information</a></li>
	    <li><a href="/pages/careers">Careers</a></li>
	    <li><a href="/Content/Forms/ContactMe.aspx">Contact Us</a></li>
	</ul>
	<div class="clear"></div>
    <div class="home-search">
	    <form class="search-form" onsubmit="return prepareSubmit();" method="post" action="/Public/Listing/ProcessSearch.aspx?" name="findahome">
	        <input class="search-text" type="text" name="Criteria/LocationBox" value="City, Zip, MLS #, Neighborhood, Street Address" />
	        <div class="search-submit"><input type="submit" value="" name="submit" /></div>
	        <div class="clear"></div>
            <input type="hidden" value="map" name="Criteria/SearchType" />
            <input type="hidden" id="SearchTab" value="1" name="SearchTab" />
            <input type="hidden" id="AddressGoto" value="1" name="AddressGoto" />
            <input type="hidden" name="Criteria/LocationValue" id="LocationValue" /><input type="hidden" id="LocationBoxType" name="Criteria/LocationType" />
            <input type="hidden" id="Location" name="Criteria/Location" />
            <input type="hidden" name="Criteria/BoundaryID" />
	    </form>
	    <img class="right-house" src="http://caroljonesimages-stage.fnistools.com/Uploads/RECos/35002/ContentFiles/right-house.png" alt="Right House, Right Price, Right NOW.">
    </div>
</div>
<div class="clear"></div>
    <div class="gray-arrow"></div>
<div class="clear"></div>