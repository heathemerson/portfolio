<?php include('includes/header.php'); ?>
<div class="container_12">
	<div class="grid_8">
	    <h2 class="center">CJR Veterans to Realtors Program</h2>
	    <p class="center">"Veterans are the lifeblood of this great country and the very reason we enjoy our freedom. This is a small token of our appreciation for their service."</p>
        <p class="center">- Shaun Duggins, CEO, Carol Jones Realtors</p>
	</div>
    <div class="grid_4 subcontent-image">
	    <img src="http://caroljonesimages-stage.fnistools.com/Uploads/RECos/35002/ContentFiles/vets-to-realtors.jpg" alt="CJR Veterans to Realtors Program." />
	</div>
</div>

<hr />

<div class="container_12">
	<div class="grid_8">
	    <h3 class="center">Mortgage Pre-Approval</h3>
	    <img src="http://placehold.it/620x300" alt="" />
	</div>
    <div class="grid_4 content-pad-top">
        <h4>Local Market Trends</h4>
	    <img src="http://placehold.it/300x127" alt="" />
        <h4>Find Agents or Offices</h4>
	    <img src="http://placehold.it/300x127" alt="" />
	</div>   
</div>

<hr />

<div class="container_12">
    <h2 class="center">My Home Alert Signup</h2>
    <p class="center">Sed suscipit sodales sodales. Sed mi libero, sollicitudin sed volutpat eu, sodales fermentum est. Vestibulum sagittis massa ac tellus tempus, dapibus congue ligula tempus. Donec vel pulvinar elit. Aenean convallis ullamcorper libero, sagittis rutrum enim elementum.</p>
    <form class="home-alert">
        <div class="field-group">
            <label for="Email Address">Email Address:</label>
            <input type="text" name="email" value="" />
        </div>
        <div class="field-group">
	        <label for="Password">Password:</label>
	        <input type="password" name="password" value="" />
        </div>
        <input class="submit avenir-bold" type="submit" name="submit" value="Sign Up!" />
    </form>
</div>

<hr />

<div class="container_12">
	<div class="grid_8">
	    <h6 class="center">HSA Home Warranty and You ... Together, We're Better!</h6>
	    <p class="center">HSA provides comprehensive, affordable home warranty coverage insurace
with customer service that's second to none. Learn more...</p>
	</div>
    <div class="grid_4 subcontent-image">
	    <img src="http://caroljonesimages-stage.fnistools.com/Uploads/RECos/35002/ContentFiles/hsa-warranty.jpg" alt="CJR Veterans to Realtors Program." />
	</div>
</div>

<hr />

<div class="container_12">
    <h2 class="center">Residential Real Estate</h2>
    <ul class="grid_3 no-bull-list">
        <li><a href="/MO/Springfield">Springfield real estate</a></li>
        <li><a href="/MO/Nixa">Nixa real estate</a></li>
        <li><a href="/MO/Ozark">Ozark real estate</a></li>
        <li><a href="/MO/Republic">Republic real estate</a></li>
    </ul>
    <ul class="grid_3 no-bull-list">
        <li><a href="/MO/Rogersville">Rogersville real estate</a></li>
        <li><a href="/MO/Seymour">Seymour real estate</a></li>
        <li><a href="/MO/Bolivar">Bolivar real estate</a></li>
        <li><a href="/MO/Mount-Vernon">Mount Vernon real estate</a></li>
    </ul>
    <ul class="grid_3 no-bull-list">
        <li><a href="/MO/Branson">Branson real estate</a></li>
        <li><a href="/MO/Forsyth">Forsyth real estate</a></li>
        <li><a href="/MO/Kimberling-City">Kimberling City real estate</a></li>
        <li><a href="/MO/Hollister">Hollister real estate</a></li>
    </ul>
    <ul class="grid_3 no-bull-list">
        <li><a href="/MO/Reeds-Spring">Reeds Spring real estate</a></li>
        <li><a href="/MO/Shell-Knob">Shell Knob real estate</a></li>
        <li><a href="/MO/West-Plains">West Plains real estate</a></li>
        <li><a href="/MO">Missouri real estate</a></li>
    </ul>
</div>

<hr />

<div class="container_12">
    <div id="company-copyright"> 
        <a href="http://qsc4.me/c/2000233"><img src="http://qsc4.me/cw2/2000233.png" width="260" height="80" border="0" alt="QSC" /></a><br /><br />
        &copy; 2001-2012 Carol Jones Realtors, 
        A Missouri Corporation.<br>
        3600 S. National Ave. Springfield, MO 65807 - (800) 870-7892<br>
        A HomeServices of America, Inc. company and Berkshire Hathaway affiliate. 
        All rights reserved.
        <div id="terms-and-conditions">
            <a href="/pages/terms-conditions">Terms and Conditions</a> | 
            <a href="/pages/privacy-policy">Privacy Policy</a> | 
            <a href="/pages/disclaimer">Disclaimer</a>
        </div>
    </div>
</div>

<?php include('includes/footer.php'); ?>