<?php include('includes/header.php'); ?>
<div class="container_12">
    <h2 class="center">Buying A Home</h2>
</div>
<hr />
<div class="container_12">
    <div class="grid_4 subcontent-image">
	    <img src="http://placehold.it/270x300" alt="" />
	</div>
    <div class="grid_7 pad-left">
	    <h5 class="avenir-bold">Buying A Home</h5>
	    <p>First time buyer? Searching for your dream home? Not completely sure what you're looking for? At CJR, our job is to help you find the right home at the right price, right now. We do that by committing over 25 years of professional real estate service to YOUR needs and desires.
Your CJR Buyer's Agent will represent you—and only you—in all real estate matters. He or she will show you any property (no matter whose name is on the sign), assist you in making an offer to purchase it and then negotiate on your behalf. Even after your offer is accepted, your agent will keep you fully informed while working with your lender, inspector and the title company all the way up until that final, exciting moment when they hand you the keys to your new home.
And if you have questions about topics like renting vs. buying, information about short sales or help with down payments, our experienced team at CJR can help answer all of your questions and more.</p>
        <p>So if you're looking for a home in southern Missouri, now's the perfect time to buy. Go ahead and search our listings below or call our office and let CJR help you find the right Buyer's Agent and the right home – right now.</p>

	</div>
</div>
<?php include('includes/footer.php'); ?>