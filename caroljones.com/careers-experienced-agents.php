<?php include('includes/header.php'); ?>
<div class="container_12">
    <h2 class="center">Careers for Experienced Agents</h2>
</div>
<hr />
<div class="container_12">
    <div class="grid_4 subcontent-image">
	    <img src="http://placehold.it/270x300" alt="" />
	</div>
    <div class="grid_7 pad-left">
	    <h5 class="avenir-bold">Experienced Agents</h5>
	    <p>You are an industry veteran, which means you know what to expect. It also means you know what you've been missing. CJR is committed to providing the tools you need to be successful in today’s market environment. We welcome new ideas and encourage our agents to think outside the box. Besides free CE for all of our agents, CJR is proud to be the only area brokerage to offer our agents insurance. Why? Because we work hard to support our most important asset – our agents.</p>
        <p>For over 25 years, CJR has worked hard to establish itself as the premiere real estate company when it comes to ongoing training. Whether it’s learning new tools, mastering new technology or just getting better at the things you’re already good at, CJR has everything you need to build your career and expand your horizons.</p>
	</div>
</div>
<?php include('includes/footer.php'); ?>