<?php include('includes/header.php'); ?>
<div class="container_12">
    <h2 class="center">Area Information</h2>
</div>
<hr />
<div class="container_12">
    <div class="grid_4 subcontent-image">
	    <img src="http://placehold.it/270x300" alt="" />
	</div>
    <div class="grid_7 pad-left">
	    <h5 class="avenir-bold">Discover Your Ideal Area</h5>
	    <p>With so many great communities in southwest Missouri, the hardest part may be deciding exactly where you want to live. No one will know more about local area information than your CJR agent, but we’re more than happy to provide any and all information we can provide when it comes to choosing the location of your new home.</p>
 
        <p>Just use our search tools below to get the information you're looking for. And if you'd like to know more about a specific community, give us a call today. Our agents are experts when it comes to your new neighborhood, and we'd be glad to answer any questions you have.</p>
	</div>
</div>
<hr />
<div class="container_12">
    <div class="grid_9">
        <h5 class="center">Community Search</h5>
        <p>To learn more about specific neighborhoods and communities in southwest Missouri, you can use our Community Search function below. It offers all kinds of information, including population and age demographics, households with children and more.</p>

        <p>Just search through the communities you’re considering to see for yourself!</p>
    </div>
    <div class="grid_3 content-pad-top center">
        <a href="/pages/community-info" class="med-btn">Search</a>
    </div>
</div>
<hr />
<div class="container_12">
    <div class="grid_9">
        <h5 class="center">Schools Search</h5>
        <p>If you're interested in which schools your children will be attending in your new community, use our Schools Search to find the information you’re looking for. From school class size to student-to-teacher ratios, our Schools Search should have all the info you need.</p>
    </div>
    <div class="grid_3 content-pad-top center">
        <a href="/pages/community-info" class="med-btn">Search</a>
    </div>
</div>
<hr />
<div class="container_12">
    <p class="center">If you're interested in which schools your children will be attending in your new community, use our Schools Search to find the information you’re looking for. From school class size to student-to-teacher ratios, our Schools Search should have all the info you need.</p>
    <p class="center">Give us a call or send us an email today for more info!</p>
</div>
<?php include('includes/footer.php'); ?>