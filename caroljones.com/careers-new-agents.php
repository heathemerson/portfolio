<?php include('includes/header.php'); ?>
<div class="container_12">
    <h2 class="center">Careers for New Agents</h2>
</div>
<hr />
<div class="container_12">
    <div class="grid_4 subcontent-image">
	    <img src="http://placehold.it/270x300" alt="" />
	</div>
    <div class="grid_7 pad-left">
	    <h5 class="avenir-bold">New Licensees</h5>
	    <p>At CJR, we can help you navigate real estate school, license testing, fingerprinting and applications to all appropriate associations and boards. It’s one thing to earn your license; earning a living with your license is another thing entirely.</p>
        <p>The bottom line is that you need to get going – fast. Our team at CJR will teach you how to:</p>
        <ul class="bul-list grid_7 push-1">
            <li>Market yourself</li>
            <li>Prospect for business</li>
            <li>Set goals</li>
            <li>Run your own business</li>
            <li>and more!</li>
        </ul>
	</div>
</div>
<hr />
<div class="container_12">
    <p class="center grid_">Combine all that with experienced mentors who provide one-on-one training, and it’s plain to see why CJR is the best place to get your start in real estate.</p>
</div>
<?php include('includes/footer.php'); ?>