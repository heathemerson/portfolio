<?php include('includes/header.php'); ?>
<div class="container_12">
    <h2 class="center">Relocation Services</h2>
</div>
<hr />
<div class="container_12">
    <div class="grid_4 subcontent-image">
	    <img src="http://placehold.it/270x300" alt="" />
	</div>
    <div class="grid_7 pad-left">
	    <h5 class="avenir-bold">Relocation Services from Carol Jones</h5>
	    <p>We know it's not always easy to move. For some people, the actual act of moving can be more stressful than the entire home selling process.</p>
        <p>Fortunately, CJR offers an unparalleled relocation service to help you make sure every step of the moving process is handled efficiently and professionally. Whether you're moving across town or moving to a whole new community, CJR's relocation service ensures that you'll never have to stress about moving.</p>
        <p>We work with a number of experts who have been through the relocation process many times, and they're trained to help you navigate the choppy waters of moving to a new community from another city, state, or even country. Whether it's just you and your family making a move or it's your entire business, our relocation service team knows how to set you up for success in your new community.</p>
	</div>
</div>
<hr />
<div class="container_12">
    <h5 class="center">Some of the excellent relocation service providers we work with include:</h5>
    <div class="grid_3 push_1">
        <ul class="bul-list">
            <li>Home Services Relocation</li>
            <li>Cartus</li>
            <li>Sirva</li>
        </ul>
    </div>
    <div class="grid_3 push_2">
        <ul class="bul-list">
            <li>Primacy</li>
            <li>Corporate Relocation Service</li>
            <li>Werchert</li>
        </ul>
    </div>
    <div class="grid_3 push_3">
        <ul class="bul-list">
            <li>Brookfield</li>
            <li>Lexicon</li>
            <li>and Paragon</li>
        </ul>
    </div>
</div>
<hr />
<div class="container_12">
    <p class="center">Our expert relocation service is just one of the many advantages you get when you work with a team that has over 25 successful years in the real estate industry. That’s what you get when you work with CJR. For more information about our Relocation Services, send us an email or give us a call today at 417-883-6666 or 1-800-870-7892.</p>
</div>
<?php include('includes/footer.php'); ?>