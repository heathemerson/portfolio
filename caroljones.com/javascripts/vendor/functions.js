// Submit button
$('.submit').each(function(){
    $(this).hide().after('<span class="submit">').next('span.submit').text($(this).val()).click(function(){
        $(this).prev('input.submit').click();
    });
});

// Clear input default value on focus
$(document).ready(function(){ 
    $("input[type=text]").focus(function() {
        if(this.value == this.defaultValue) {
             this.value = "";
        }
    }).blur(function(){
        if(this.value.length == 0) {
           this.value = this.defaultValue;
        }
    });
})