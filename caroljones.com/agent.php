<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head><!-- Build Number: 21.3.306 | Server: SWBRWSTG-OLA01 | Design: /RECoTemplates/Subpage14 -->

<meta content="Search for Homes in Missouri | CJR Carol Jones Realtors" name="title" />
<meta content="15 yr fixed, 30 yr fixed, missouri mortgage rates, springfield mo real estate, homes for sale, houses for sale, realtors, homes for sale in the, sale homes, for sale homes, realty, property for sale, sale houses, realtors, realtors in" name="keywords" />
<meta content="Find homes for sale, find an agent, view virtual tours, receive homes by e-mail, learn about buying and selling a home and more!" name="description" />
<meta content="Search for real estate and homes in your area!" name="abstract" />
<meta content="info@caroljones.com" name="reply-to" />
<meta content="noodp, noydir" name="robots" />
<meta content="All Contents Copyright &amp;#169; 2004 Carol Jones Realtors.  All Rights Reserved.The programming and software materials herein are copyright Real Estate Digital (RED). The programming and software materials are owned, held, or licensed by RED. Personal, educational, non-commercial, commercial or any other use of these materials, without the written permission of the RED, is strictly prohibited." name="copyright" />
<meta content="text/html; charset=UTF-8" http-equiv="content-type" />

<link href="/" rel="canonical" /><!-- canonical -->

<link href="http://caroljones.rdeskwebsite-stage.com/jscss/21.3.306/css/fe27bac8-9286-402c-9efa-c3a3abd9d69b" rel="stylesheet" type="text/css" /><!-- css-global -->
<link href="http://caroljones.rdeskwebsite-stage.com/jscss/21.3.306/css/6f09b613-6714-4f12-a617-dc722a678326" rel="stylesheet" type="text/css" /><!-- css-reco -->

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script><!-- js-global-ext-1 -->
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js"></script><!-- js-global-ext-2 -->
<script type="text/javascript" src="http://caroljones.rdeskwebsite-stage.com/jscss/21.3.306/js/949ec361-9ad9-4138-ac33-19ed26d32dad"></script><!-- js-global-int -->
<script type="text/javascript" src="http://caroljones.rdeskwebsite-stage.com/jscss/21.3.306/js/60d41b26-d839-41da-b163-20eb76d96ec9"></script><!-- js-page-int -->
<script type="text/javascript">
<!--//<![CDATA[
Utils.AppPath = '';
                  Utils.PublicAppName = '';
                  Utils.PublicHostHeader = 'caroljones.rdeskwebsite-stage.com';
                  if (typeof rbw == 'undefined') { var rbw = {} }
                  if (typeof rbw.utils == 'undefined') { rbw.utils = {} }
                  rbw.utils.version = '21.3.306';
                  if (typeof rbw.context == 'undefined') { rbw.context = {} }
                  rbw.context.Listing = null;
                  rbw.context.mapProvider = 'jqbingmap';
                  rbw.context.branding = 'RECo';
                  rbw.context.validStamp = 'ibrsv5pC0wKJiHcKpeJJyNnQlG4Jh0VJKWUeMp8qU0Y=';
                  rbw.context.useOfficeApi = false;
                  rbw.context.isAuthenticated = false;
                  jQuery(function() { try { jQuery('form').append('<input type="hidden" name="validstamp" value="ibrsv5pC0wKJiHcKpeJJyNnQlG4Jh0VJKWUeMp8qU0Y=" />'); } catch(err) { } });
var BulletOn = new Image();
                            BulletOn.src = 'http://caroljonesimages-stage.fnistools.com/images/RECos/35002/dropdown_bullet.gif';
                            var BulletOff = new Image();
                            BulletOff.src = 'http://caroljonesimages-stage.fnistools.com/images/RECos/35002/fill.gif';
                            var orientation = '';
//]]>-->
</script><!-- js-page-block -->

<meta http-equiv="X-UA-Compatible" content="IE=EDGE" /> <!-- Do not remove - Prevents ie9 from going into compatibility mode for new users -->
<meta name="google-site-verification" content="sLdn4xsUsJbrTZ4dO-oZjfKhL2D0oIT4sZEM0Eaq7zE" />

<link rel="shortcut icon" href="http://caroljones.rdeskwebsite-stage.com/images/recos/35002/favicon.ico" />

<!--[if gte IE 9]>
  <style type="text/css">
    * {
       filter: none;
    }
  </style>
<![endif]-->

    <link rel="stylesheet" href="stylesheets/app.css" />
    <script src="javascripts/vendor/custom.modernizr.js"></script>
</head>
<body>

<div class="agent-bg">
    <div class="bg-white-trans-80">
        <div class="container_12">
        	<div class="grid_4">
        		<a href=""><img class="cjr-logo" src="images/cjr-trilakes-logo.png" alt="Carol Jones Realtors" /></a>
        	</div>
        	<div class="grid_5 center">
        	    <div class="site-card">
            	    <div class="card-img">
                        <img src="images/joanne-anderson.jpg" alt="" />
            	    </div>
                    <div class="card-text">
                    	<h5 class="center avenir-bold">Joanne Anderson</h5>
                    	<p class="center">417-335-5950<br />
                    	<a href="mailto:info@cjrtrilakes.com">janderson@cjrtrilakes.com</a></p>
            	    </div>
        	    </div>
        	</div>
            <div class="grid_3">
        		<p class="home-alert-txt"><span>My Home Alert: </span>
        		    <a href="">Sign In</a> |
        		    <a href="">Sign Up</a>
                </p>
                <div class="social-media-icons">
                    <a href=""><span class="fb"></span></a>
                    <a href=""><span class="twitter"></span></a>
                    <a href=""><span class="linkedin"></span></a>
                    <a href=""><span class="youtube"></span></a>
                    <a href=""><span class="rss"></span></a>
                </div>
        	</div>
        </div>
    </div>
	<ul class="main-nav">
	    <li><a href="">Find a Property</a></li>
	    <li><a href="">Real Estate Services</a></li>
	    <li><a href="">Mortgage Services</a></li>
	    <li><a href="">Area Information</a></li>
	    <li><a href="">Careers</a></li>
	    <li><a href="">Contact</a></li>
	</ul>
    <div class="home-search">
        <div class="container_12">
        	<div class="grid_8">
        	    <form class="search-form">
        	        <input class="search-text" type="text" value="City, Zip, MLS #, Neighborhood, Street Address" />
        	        <div class="search-submit"><input type="submit" value="" name="submit" /></div>
        	        <div class="clear"></div>
        	    </form>
        	    <img class="right-house" src="images/right-house.png" alt="Right House, Right Price, Right NOW." />
        	</div>
        </div>
    </div>
</div>
<span id="slider-prev"></span><span id="slider-next"></span>
<ul class="bxslider">
  <li>
    <div class="container_12">
	    <div class="grid_3">
	        <div class="list-item"><a href="">1.1</a></div>
	    </div>
	    <div class="grid_3">
	        <div class="list-item"><a href="">1.2</a></div>
	    </div>
	    <div class="grid_3">
	        <div class="list-item"><a href="">1.3</a></div>
	    </div>
	    <div class="grid_3">
	        <div class="list-item"><a href="">1.4</a></div>
	    </div>
    </div>
  </li>
  <li>
    <div class="container_12">
	    <div class="grid_3">
	        <div class="list-item"><a href="">2.1</a></div>
	    </div>
	    <div class="grid_3">
	        <div class="list-item"><a href="">2.2</a></div>
	    </div>
	    <div class="grid_3">
	        <div class="list-item"><a href="">2.3</a></div>
	    </div>
	    <div class="grid_3">
	        <div class="list-item"><a href="">2.4</a></div>
	    </div>
    </div>
  </li>
  <li>
    <div class="container_12">
	    <div class="grid_3">
	        <div class="list-item"><a href="">3.1</a></div>
	    </div>
	    <div class="grid_3">
	        <div class="list-item"><a href="">3.2</a></div>
	    </div>
	    <div class="grid_3">
	        <div class="list-item"><a href="">3.3</a></div>
	    </div>
	    <div class="grid_3">
	        <div class="list-item"><a href="">3.4</a></div>
	    </div>
    </div>
  </li>
  <li>
    <div class="container_12">
	    <div class="grid_3">
	        <div class="list-item"><a href="">4.1</a></div>
	    </div>
	    <div class="grid_3">
	        <div class="list-item"><a href="">4.2</a></div>
	    </div>
	    <div class="grid_3">
	        <div class="list-item"><a href="">4.3</a></div>
	    </div>
	    <div class="grid_3">
	        <div class="list-item"><a href="">4.4</a></div>
	    </div>
    </div>
  </li>
</ul>

<div class="gray-arrow"></div>

<div class="container_12">
	<div class="grid_8">
	    <h2 class="center">CJR Veterans to Realtors Program</h2>
	    <p class="center">"Veterans are the lifeblood of this great country and the very reason we enjoy our freedom. This is a small token of our appreciation for their service."</p>
        <p class="center">- Shaun Duggins, CEO, Carol Jones Realtors</p>
	</div>
    <div class="grid_4 subcontent-image">
	    <img src="http://caroljonesimages-stage.fnistools.com/Uploads/RECos/35002/ContentFiles/vets-to-realtors.jpg" alt="CJR Veterans to Realtors Program." />
	</div>
</div>

<hr />

<div class="container_12">
	<div class="grid_8">
	    <h3 class="center">Mortgage Pre-Approval</h3>
	    <img src="http://placehold.it/620x300" alt="" />
	</div>
    <div class="grid_4 content-pad-top">
        <h4>Local Market Trends</h4>
	    <img src="http://placehold.it/300x127" alt="" />
        <h4>Find Agents or Offices</h4>
	    <img src="http://placehold.it/300x127" alt="" />
	</div>
</div>

<hr />

<div class="container_12">
    <h2 class="center">My Home Alert Signup</h2>
    <p class="center">Sed suscipit sodales sodales. Sed mi libero, sollicitudin sed volutpat eu, sodales fermentum est. Vestibulum sagittis massa ac tellus tempus, dapibus congue ligula tempus. Donec vel pulvinar elit. Aenean convallis ullamcorper libero, sagittis rutrum enim elementum.</p>
    <form class="home-alert">
        <div class="field-group">
            <label for="Email Address">Email Address:</label>
            <input type="text" name="email" value="" />
        </div>
        <div class="field-group">
	        <label for="Password">Password:</label>
	        <input type="password" name="password" value="" />
        </div>
        <input class="submit avenir-bold" type="submit" name="submit" value="Sign Up!" />
    </form>
</div>

<hr />

<div class="container_12">
	<div class="grid_8">
	    <h6 class="center">HSA Home Warranty and You ... Together, We're Better!</h6>
	    <p class="center">HSA provides comprehensive, affordable home warranty coverage insurace
with customer service that's second to none. Learn more...</p>
	</div>
    <div class="grid_4 subcontent-image">
	    <img src="http://caroljonesimages-stage.fnistools.com/Uploads/RECos/35002/ContentFiles/hsa-warranty.jpg" alt="CJR Veterans to Realtors Program." />
	</div>
</div>

<hr />

<div class="container_12">
    <div id="company-copyright">
        <a href="http://qsc4.me/c/2000233"><img src="http://qsc4.me/cw2/2000233.png" width="260" height="80" border="0" alt="QSC" /></a><br /><br />
        &copy; 2001-2012 Carol Jones Realtors,
        A Missouri Corporation.<br>
        3600 S. National Ave. Springfield, MO 65807 - (800) 870-7892<br>
        A HomeServices of America, Inc. company and Berkshire Hathaway affiliate.
        All rights reserved.
        <div id="terms-and-conditions">
            <a href="/pages/terms-conditions">Terms and Conditions</a> |
            <a href="/pages/privacy-policy">Privacy Policy</a> |
            <a href="/pages/disclaimer">Disclaimer</a>
        </div>
    </div>
</div>
<script src="javascripts/vendor/jquery.bxslider.min.js"></script>
<script>
    // Bx Slider
    $('.bxslider').bxSlider({
      nextSelector: '#slider-next',
      prevSelector: '#slider-prev',
      nextText: '→',
      prevText: '←',
      pager: false
    });
</script>
<?php include('includes/footer.php'); ?>
