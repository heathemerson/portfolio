<?php include('includes/header.php'); ?>
<div class="container_12">
    <h2 class="center">CJR Careers</h2>
</div>
<hr />
<div class="container_12">
    <div class="grid_6 center">
        <a href="/pages/careers-new-agents" class="no-decor">
        <img class="rounded-img-large" src="http://placehold.it/290x290" alt="" />
        <h5 class="avenir-bold">New Agents</h5>
        </a>
    </div>
    <div class="grid_6 center">
        <a href="/pages/careers-experienecd-agents" class="no-decor">
        <img class="rounded-img-large" src="http://placehold.it/290x290" alt="" />
        <h5 class="avenir-bold">Experienced Agents</h5>
        </a>
    </div>
</div>
<hr />
<div class="container_12">
    <h2 class="center">Working With Carol Jones Realtors</h2>
    <p class="center">Whether you’re just getting started in the real estate industry or you’re a REALTOR® with years of experience, CJR has the tools, resources and training opportunities to help you reach the top of the real estate food chain – and stay there. Choose your career path above.</p>
</div>
<hr />
<div class="container_12">
    <div class="grid_4 subcontent-image">
	    <img src="http://placehold.it/270x300" alt="" />
	</div>
    <div class="grid_7 pad-left">
	    <h5 class="avenir-bold">CJR Offers More</h5>
	    <p>CJR has 8 area locations, giving us excellent regional coverage. More opportunity equals more money, and with CJR you’ll have an opportunity to do business everywhere.
CJR’s stellar relocation service also benefits agents, providing a robust database of lead and referral opportunities. So if you are interested in working with CJR as an agent, our relocation service is just one of many incentives to join the Carol Jones team.
Most importantly, when you work with CJR, you work with a team of real estate stars who will help you improve on a daily basis. Our expectations are high and we expect our agents to succeed through an emphasis on quality. Not quantity – quality. On top of that, working with CJR gives you an opportunity to surround yourself with other prime players in the real estate industry.</p>
        <p>So if you’re looking for a career in real estate, come work for a team that provides the resources you need to find REAL success. Call us at 417-883-6666 or email us today* to see how you can get started at CJR – and take your career to a whole new level along the way.</p>
        <p>*Please know that ALL of our inquiries are completely confidential, meaning you can safely look into a career at CJR without the fear of repercussion from your current employer.</p>
	</div>
</div>
<?php include('includes/footer.php'); ?>