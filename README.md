# Portfolio

### BrandJelly.com - Mockup
*  Brainstorming site layout and design.

### CarolJones.com
*  Designed and developed website.
*  Integrated website into rDesk platform. (https://realestatedigital.com)
*  Note: CarolJones is now owned and operated by ReeceNichols.

### Coalition - Test
*  One hour test to attempt to code site from provided PSD.

### DropsofSynergy.com - Mockup
*  Brainstorming site layout and design.
*  Designed logo and coded site in one evening.

### EvntLive.com
*  Coded site from provided artwork files.

### GigSalad - Test
*  A test to develop a page that lists gigs.
*  Coded in CodeIgniter, React, and used Webpack.

### Vets.CarolJones.com
*  Designed and developed website.
*  Integrated website into rDesk platform. (https://realestatedigital.com)
*  Note: CarolJones is now owned and operated by ReeceNichols.
