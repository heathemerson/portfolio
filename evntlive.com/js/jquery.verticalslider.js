// Set variables
var controls = $('#controls li.slidetab'),
    controlElements = $('#controls li a'),
    minHeight = 625,
    minWidth = 1000,
    heightPadding = 60,
    windowHeight = $(window).height(),
    windowWidth = $(window).width(),
    panelWrapper = $('#panelWrapper'),
    panelSlide = $('#panelSlide'),
    panelContainer = $('.panelContainer'),
    panelSlideshow = $('#panelSlideshow'),
    slideshowTabs = $('#slideshowTabs'),
    autoSlideTimer = true,
    slideTimer = 8000;

function controlsTooltip() {
    $(controlElements).each(function() {
        controlsText = $(this).attr('title');
        textPositionTop = $(this).parent().position().top + 3;
        textPositionLeft = $(this).parent().position().left;
        //console.log(textPositionLeft);
        if (controlsText)
        $(this).parent().append('<div class="controlsTooltip" style="top: '+textPositionTop+'px;">'+controlsText+'</div>');
        
        var currentTooltip = $(this).parent().find('.controlsTooltip');

        $(this).removeAttr('title');

        $(this).hover(function(){
            $(currentTooltip).show();
        }, function () {
            $(currentTooltip).hide();
        });
    });
}
controlsTooltip();

function resizeFloor() {
    var watchFloorContainer = $('#watch .floorContainer'),
    connectFloorContainer = $('#connect .floorContainer');
    if ($('#watch').hasClass('active')) {
        var watchFloorTop = $('#watch.active .panelSubContentContainer').offset().top - 177 - heightPadding;
        $('#watch .floorContainer').css('top', watchFloorTop);
    }
    if ($('#connect').hasClass('active')) {
        var connectFloorTop = $('#connect.active .panelSubContentContainer').offset().top - 177 - heightPadding;
        $('#connect .floorContainer').css('top', connectFloorTop);
    }
}


function resizeSlideshow(windowHeight, windowWidth) {
    var currentTab = $('#slideshowTabs a.current').index();
    var currentSlide = $('#panelSlideshow img');

    slideshowHeight = $(currentSlide[currentTab]).height() - heightPadding;
    $(panelSlideshow).css('height', slideshowHeight);

    var tabsPositionTop = slideshowHeight + heightPadding - 5; 
    var tabsPositionLeft = (($(currentSlide[currentTab]).width() / 2) + 250 + ($(slideshowTabs).width()/2));
    $(slideshowTabs).css({'top': tabsPositionTop, 'left': tabsPositionLeft});
}
 
function resizePanels(windowHeight, windowWidth) {
    if (minHeight < windowHeight) {
        var windowHeightPadding = windowHeight + heightPadding;
        $(panelWrapper).css('height', windowHeightPadding);
        $(panelSlide).css('height', windowHeightPadding);
        $(panelContainer).css('height', windowHeightPadding);
    } else {
        var minHeightPadding = minHeight + heightPadding;
        $(panelWrapper).css('height', minHeightPadding);
        $(panelSlide).css('height', minHeightPadding);

        $(panelContainer).css('height', minHeightPadding);

    }
    $(panelContainer).each(function(e){
        if ($(panelContainer[e]).hasClass('active')) {
            var containerHeight = $(panelContainer).height();
            var panelTop = e * containerHeight; //Set the offet
            panelTop = -panelTop;
            panelTop = panelTop + heightPadding;
            $(panelSlide).css('top', panelTop);
        }
    });
}

function resizeEvents() {
    windowHeight = $(window).height();
    windowWidth = $(window).width();
    resizeSlideshow(windowHeight, windowWidth);
    resizePanels(windowHeight, windowWidth);
    resizeFloor();
}

$(window).resize(function() {
    resizeEvents();
});

resizeEvents();

// Update slide based on control change
$.fn.changeSlide = function() {
    var $anchor = $(this).find('a');
    if ($anchor.attr('href')) {
        if ($anchor.attr('href') == '#connect') {
            //$(".slideshowTabs").data("slideshow").play();
        }
        var scrollPosition = $($anchor.attr('href')).offset().top - heightPadding;
        $(controls).removeClass('active');
        $(panelContainer).removeClass('active');
        $(panelSlide).stop().animate({
            top: '-='+scrollPosition
        }, 1000, 'easeInOutExpo', function() {
            resizeEvents(); 
        });
        $($anchor.attr('href')).addClass('active');
        $($anchor).parent().addClass('active');
    }
 };

// Control bullets
$(controls).click(function(event){
    $(this).changeSlide();
    event.preventDefault();
});

// Enable scroll function

function mouseScroll() {
    /*
    var previousScroll = 0;
    var currentScroll = $(this).scrollTop();
    if (currentScroll > previousScroll){
        $('#controls li a.active').parent().next().find('a').stop().changeSlide();
    } else {
        $('#controls li a.active').parent().prev().find('a').stop().changeSlide(); 
    }
    
   previousScroll = currentScroll;
   */
   event.preventDefault();

}

function autoSlide() {
    nextControl = $('#controls li.slidetab.active').next('li.slidetab');
    restartControl = $('#controls li.slidetab:first');
    if (autoSlideTimer == true) {
        if ($(nextControl).length) {
            nextControl.stop().changeSlide();
        } else {
            restartControl.stop().changeSlide();
        }
    }
    if (autoSlideTimer == true) {
        setTimeout('autoSlide()', slideTimer);
    }
}

setTimeout('autoSlide()', slideTimer);

$(window).click(function() {
    autoSlideTimer = false;
});

$(window).bind('scroll', function(){
    mouseScroll();
});

// Enable touch device swiping
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
    $("body").wipetouch({
        //wipeLeft: function(result) { },
        //wipeRight: function(result) { },
        wipeUp:   function(result) {  $('#controls li a.active').parent().next().find('a').stop().changeSlide();},
        wipeDown: function(result) {  $('#controls li a.active').parent().prev().find('a').stop().changeSlide();},
    });
}
