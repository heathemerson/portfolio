#
#################
# Gigsalad Test Project
###################

Setup:

- Unzip to web directory. Note: I pointed a local domain called: "http://gigsalad.test" to the directory.

- Edit: application -> config -> database.php
	-	'username' => 'gigsaladtest',
	-	'password' => 'gigsaladtest12345',
	-	'database' => 'gigsalad-test',

- Load database tables from performers.sql.zip

- In command line from the web root directory, type:
	-	npm -i
	-	npm run build
