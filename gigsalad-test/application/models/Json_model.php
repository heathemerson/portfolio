<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Json_model extends CI_Model {

        function Json_model() {
                $this->load->database();
        }
        
        public function limit_performers($limit,$offset)
        {
                $this->db->select('*');
                $this->db->from('performers');
                $this->db->limit($limit,$offset);
                return json_encode($this->db->get()->result());
        }
}