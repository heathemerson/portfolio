<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Json extends CI_Controller {

	/*
	* Limit Performers
	*/
    public function performers($limit,$offset)
    {
            $this->load->model('Json_model');

            header('Content-type: application/json');

            echo $this->Json_model->limit_performers($limit,$offset);
    }
}
 