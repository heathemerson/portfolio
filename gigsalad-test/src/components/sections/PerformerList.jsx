import React from "react";
import ListPerformer from "./ListPerformer.jsx";

function PerformerList(props) {
  let performers = props.performers;
  let listPerformers = performers.map((performer) =>
    <ListPerformer key={performer.ID} data={performer || {}} />
  );
  return (
    <div className="grid-x grid-padding-x small-up-1 medium-up-3">
      {listPerformers}
    </div>
  );
}

export default PerformerList;