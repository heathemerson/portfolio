import React from "react";

function ListPerformer(props) {
  let thumbnail = JSON.parse(props.data.thumbnail);
  return (
    <div className="cell user-card">
      <a href={props.data.url}><img className="user-card__image-profile" src={thumbnail.url} alt={props.data.act_name} /></a>
      <h4 className="user-card__act-name">{props.data.act_name}</h4>
      <p>{props.data.category_name}</p>
      <p>{props.data.city_name}, {props.data.state_code}</p>
      <a className="hollow button user-card__button-profile expanded" href={props.data.url}>View Profile</a>
    </div>
  );
}

export default ListPerformer;