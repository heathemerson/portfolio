import React from 'react';
import '../scss/main.scss'

import PerformerList from './sections/PerformerList.jsx';
import ListPerformer from './sections/ListPerformer.jsx';

class App extends React.Component {

    constructor() {
        super();
        this.loader = React.createRef();
        this.state = {
        	performers: [],
        	counter: 0,
            limit: 9,
            isHidden: true
        }
        this.loadPerformers = this.loadPerformers.bind(this);
    }

	toggleHidden () {
	    this.setState({
	      isHidden: !this.state.isHidden
	    })
	}

    fetchPerformers(limit = 9, counter = 0){
    	this.toggleHidden();
        fetch(`/index.php/json/performers/${limit}/${counter}`).then((response) => {
            return response.json();
        }).then((performers) => {
            this.setState({performers, limit, counter});
            this.toggleHidden();
        });
    }

    componentDidMount(){
		this.fetchPerformers();
    }

    loadPerformers(){
        this.fetchPerformers(this.state.limit + 9, this.state.counter);
    }

    render() {
        return (
            <div className="container">
              {!this.state.isHidden && <Loader />}
              <PerformerList performers={this.state.performers} />
              <button className="button medium button__load-more" onClick={this.loadPerformers}>Load More</button>
            </div>
        )
    }
}

const Loader = () => (
	<div className="loader-container"><div className="loader"></div></div>
)

export default App;